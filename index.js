// use script-loader to load nimbus library
require('script!./vendors/enseo_nimbus.js');
require('script!./vendors/enseo_player.js');
require('script!./vendors/enseo_tvcontroller.js');
require('script!./vendors/enseo_browser.js');

var EnseoNimbus = require('./lib/nimbus').default;

module.exports = EnseoNimbus;
