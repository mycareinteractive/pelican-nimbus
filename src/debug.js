var ConsoleLog = function () {};
if(console && console.log) {
    ConsoleLog = console.log;

    console.log = function() {
        var args = Array.prototype.slice.call(arguments);
        var msg = args.join(' ');
        ConsoleLog.call(console, msg);

        if(window.Nimbus.NimbusObj)
            Nimbus.logMessage.call(Nimbus, msg);
    };
}

var debug = {
    log: function() {
        var args = Array.prototype.slice.call(arguments);
        args.splice(0, 0, '[NIMBUS]');
        var msg = args.join(' ');
        console.log.call(console, msg);
    }
};

export default debug;