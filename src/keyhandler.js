var KeyCodes = require('pelican-device').KeyCodes;
var debug = require('./debug').default;

export default class KeyHandler {

    constructor(nimbus) {
        this.nimbus = nimbus;
    }

    process(event) {
        debug.log('Event received: ' + event.EventMsg + ' type: ' + event.EventType);
        var e = new $.Event();

        e.handled = undefined;

        if (event.EventType == Nimbus.Event.TYPE_COMMAND) {
            var Cmd = Nimbus.parseCommand(event.EventMsg);
            e.which = Cmd.Code;
            debug.log('Command received: Code ' + Cmd.Code + ' -> Key ' + keyMap[Cmd.Code]);
        }
        else if (event.EventType == Nimbus.Event.TYPE_NOTIFICATION) {
            debug.log("Notification received: " + event.EventMsg);
        }

        var key = keyMap[e.which];
        if (key) {
            this.nimbus.triggerKey(e, key);
            debug.log('Key: ' + key + ' handled: ' + e.handled);
        }

        if (e.handled)
            return true;
        else
            return false;
    }

}

const keyMap = {};

if(window.Nimbus) {
    keyMap[Nimbus.Command.OnOff] = KeyCodes.Power;
    keyMap[Nimbus.Command.On] = KeyCodes.Power;
    keyMap[Nimbus.Command.Off] = KeyCodes.PowerOff;
    keyMap[Nimbus.Command.Back] = KeyCodes.Back;
    keyMap[Nimbus.Command.Select] = KeyCodes.Enter;
    keyMap[Nimbus.Command.Left] = KeyCodes.Left;
    keyMap[Nimbus.Command.Up] = KeyCodes.Up;
    keyMap[Nimbus.Command.Right] = KeyCodes.Right;
    keyMap[Nimbus.Command.Down] = KeyCodes.Down;
    keyMap[Nimbus.Command.Key1] = KeyCodes.Num1;
    keyMap[Nimbus.Command.Key2] = KeyCodes.Num2;
    keyMap[Nimbus.Command.Key3] = KeyCodes.Num3;
    keyMap[Nimbus.Command.Key4] = KeyCodes.Num4;
    keyMap[Nimbus.Command.Key5] = KeyCodes.Num5;
    keyMap[Nimbus.Command.Key6] = KeyCodes.Num6;
    keyMap[Nimbus.Command.Key7] = KeyCodes.Num7;
    keyMap[Nimbus.Command.Key8] = KeyCodes.Num8;
    keyMap[Nimbus.Command.Key9] = KeyCodes.Num9;
    keyMap[Nimbus.Command.Key0] = KeyCodes.Num0;
    keyMap[Nimbus.Command.Menu] = KeyCodes.Menu;
    keyMap[Nimbus.Command.ChanMinus] = KeyCodes.ChDown;
    keyMap[Nimbus.Command.ChanPlus] = KeyCodes.ChUp;
    keyMap[Nimbus.Command.Play] = KeyCodes.Play;
    keyMap[Nimbus.Command.Pause] = KeyCodes.Pause;
    keyMap[Nimbus.Command.FastForward] = KeyCodes.Forward;
    keyMap[Nimbus.Command.FastRewind] = KeyCodes.Rewind;
    keyMap[Nimbus.Command.Stop] = KeyCodes.Stop;
    keyMap[Nimbus.Command.Mute] = KeyCodes.Mute;
    keyMap[Nimbus.Command.VolMinus] = KeyCodes.VolumeDown;
    keyMap[Nimbus.Command.VolPlus] = KeyCodes.VolumeUp;
    keyMap[Nimbus.Command.CC] = KeyCodes.CC;
    keyMap[Nimbus.Command.CCAlt] = KeyCodes.CC;
}