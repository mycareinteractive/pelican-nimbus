//import Device from 'pelican';
var jQuery = require('jquery');
import Device from 'pelican-device';
import KeyHanlder from './keyhandler';
import debug from './debug';

var HTML5VIDEOLAYER = 'nimbus-h5-video-layer';
var HTML5PLAYER = 'nimbus-h5-player';

export default class EnseoNimbus extends Device {
    constructor() {
        super();
        this.keyHandler = new KeyHanlder(this);
    }

    /**
     * Start bootstrap process
     */
    bootstrap() {
        // if we are not on Enseo, bootstrap will never be ready
        if (!window.Nimbus.NimbusObj) {
            debug.log('Nimbus device not detected, bootstrap failed.');
            this.triggerDeviceFail('Nimbus device not detected, bootstrap failed.');
            return;
        }

        Nimbus.setHandshakeTimeout(180);
        Nimbus.handshake('Aceso');

        debug.log('bootstraping device...');

        jQuery(document).ready(function(){
            // Create a media player div
            $('<div id="' + HTML5VIDEOLAYER + '"></div>').insertBefore('#app').css({
                'position': 'fixed',
                'display': 'block',
                'background': 'transparent',
                'top': '0',
                'left': '0',
                'width': '1280px',
                'height': '720px',
                'z-index': '-1'
            });
            debug.log('media player div created');
        });


        Nimbus.addEventListener(this.keyHandler.process.bind(this.keyHandler));

        Nimbus.setLogLevel("Nimbus", 4);
        Nimbus.setLogLevel("Lib", 2);
        //Nimbus.setTimeZoneMode('Name');
        //Nimbus.setTimeZoneName('PST8PDT', 'America/Los_Angeles');
        Nimbus.setConsoleEnabled(true, true);
        Nimbus.setTelnetEnabled(true, true);
        Nimbus.setAuthMode(false); // undocumented API.  App can call this again to enable Pro:Idiom

        this.triggerDeviceReady();
        debug.log('bootstrap completed successfully');
    }

    /**
     * Undocumented api just for Enseo/Nimbus
     * Start handshake timer
     * Make sure you only call this after all initialization work is done and everything's normal
     * @param appString app identifier to inform platform
     */
    startHandshake(appString) {
        appString = appString || 'Aceso';
        Nimbus.handshake(appString);
        Nimbus.setHandshakeTimeout(120);

        window.setInterval(function () {
            Nimbus.handshake(appString);
        }, 30000);
    }


    /**
     * Get device platform.
     * Device platform is the unique name for every platform. Eg: "DESKTOP", "ENSEO", "PROCENTRIC"
     * @return {string} Device platform
     */
    getPlatform() {
        return 'ENSEO';
    }

    /**
     * Get hardware model.
     * @return {string}
     */
    getHardwareModel() {
        return Nimbus.getHardwareModel();
    }

    /**
     * Get hardware version
     * @return {string}
     */
    getHardwareVersion() {
        return Nimbus.getHardwareID();
    }

    /**
     * Get hardware serial number
     * @return {string}
     */
    getHardwareSerial() {
        return Nimbus.getSerialNumber();
    }

    /**
     * Get middleware version
     * @return {string}
     */
    getMiddlewareVersion() {
        return Nimbus.getFirmwareRevision();
    }

    /**
     * Network status
     * @typedef {Device[]} NetworkStatus
     * @property {string} Device.status
     * @property {string} Device.mode "NOT_REACHABLE", "UNKNOWN", "WIRE" or "WIRELESS"
     */

    /**
     * Get all network devices
     * @return {Array}
     */
    getNetworkDevices() {
        var devices = [];
        var device_list = Nimbus.getNetworkDeviceList();
        jQuery.each(device_list, function (i, name) {
            var d = Nimbus.getNetworkDevice(name);
            if (d) {
                var device = {};
                device.name = name;
                device.status = d.getEnabled() ? 'ENABLED' : 'DISABLED';
                device.mac = d.getMAC();
                device.ip = d.getIP();
                device.gateway = d.getGateway();
                device.netmask = d.getNetmask();
                devices.push(device);
            }
        });
        return devices;
    }

    /**
     * Get power status
     * @return {boolean}
     */
    getPower() {
        var ctrl = Nimbus.getTVController();
        if (ctrl)
            return ctrl.getPower();
        return undefined;
    }

    /**
     * Set power status
     */
    setPower(state) {
        var ctrl = Nimbus.getTVController();
        if (ctrl)
            ctrl.setPower(state);
    }

    /**
     * Get volume level
     */
    getVolume() {
        var ctrl = Nimbus.getTVController();
        return ctrl.getVolume();
    }

    /**
     * Set volume level
     */
    setVolume(level) {
        var ctrl = Nimbus.getTVController();
        ctrl.setVolume(level);
    }

    /**
     * Play a tv channel
     * @param {Object} param - Parameters object
     * @param {string} param.mode - Tuning Mode, "LogicalNumber", "ChannelNumber", "Frequence", "IPAddressPort"
     * @param {string} param.type - Channel type, "IP", "Analog" or "Digital"
     * @param {string} param.rfType - RF broadcast type, "Cable" or "Air"
     * @param {number} param.majorNumber - RF channel number
     * @param {number} param.minorNumber - RF minor channel for digital signals
     * @param {string} param.ip - UDP ip address
     * @param {number} param.port - UDP port
     * @return {Object} jQuery promise
     */
    playChannel(param) {
        var defer = jQuery.Deferred();
        var content = '';

        var type = (param.type || 'Analog').toLowerCase();
        //var mode = (param.mode || 'ChannelNumber').toLowerCase();

        if (type == 'analog') {
            content = '<ChannelParams ChannelType="Analog">' +
                '<AnalogChannelParams ' +
                'PhysicalChannelIDType="%rfType%" ' +
                'PhysicalChannelID="%chanId%">' +
                '</AnalogChannelParams>' +
                '</ChannelParams>';

            param.rfType = (param.rfType == 'Air') ? 'Air' : 'Cable';
            content = content.replace('%rfType%', param.rfType)
                .replace('%chanId%', param.majorNumber);
        }
        else if (type == 'digital') {
            content = '<ChannelParams ChannelType="Digital">' +
                ' <DigitalChannelParams' +
                '  PhysicalChannelIDType="%rfType%"' +
                '  PhysicalChannelID="%chanId%"' +
                '  DemodMode="QAMAuto"' +
                '  ProgramSelectionMode="PATProgram"' +
                '  ProgramID="%programId%">' +
                ' </DigitalChannelParams>' +
                '</ChannelParams>';

            param.rfType = (param.rfType == 'Air') ? 'Air' : 'Cable';
            content = content.replace('%rfType%', param.rfType)
                .replace('%chanId%', param.majorNumber)
                .replace('%programId%', param.minorNumber);
        }
        else if (type == 'ip') {
            content = '<ChannelParams ChannelType="UDP">' +
                ' <UDPChannelParams' +
                '  Address="%ip%"' +
                '  Port="%port%">' +
                ' </UDPChannelParams>' +
                '</ChannelParams>';

            content = content.replace('%ip%', param.ip)
                .replace('%port%', param.port);
        }

        var player = Nimbus.getPriPlayer();
        if (player) {
            player.stop();
            player.setContent(content, null);
        }
        else {
            player = Nimbus.getPriPlayer(content, 0, null);
        }

        if (player) {
            player.setVideoLayerBlendingMode("page_alpha");
            player.setPictureFormat("Widescreen");
            player.setVideoLayerEnable(true);
            player.setRetryEnable(true);
            player.play();
            defer.resolve();
        }
        else {
            defer.reject();
        }

        return defer.promise();
    }


    /**
     * Stop current playing channel
     */
    stopChannel() {
        var defer = jQuery.Deferred();
        var player = Nimbus.getPriPlayer();
        if (player) {
            player.stop();
            player.close();
        }
        defer.resolve();
        return defer.promise();
    }

    _getH5Player() {
        var e = $('#' + HTML5PLAYER);
        if (e && e.length > 0)
            return e[0];
        else
            return null;
    }

    _createH5Player() {
        this._removeH5Player();
        var e = $('<video></video>').attr('id', HTML5PLAYER);
        $('#' + HTML5VIDEOLAYER).append(e);
        e[0].style.position = 'relative';
        e[0].style.width = '100%';
        e[0].style.height = '100%';
        return e[0];
    }

    _removeH5Player() {
        var e = this._getH5Player();
        if (e) {
            e.pause();
            e.src = '';
            e.load();
            $(e).remove();
        }
    }

    /**
     * Play a media stream
     * @param {Object} param - Parameter object
     * @param {string} param.url - Video URL
     * @param {string} param.mimeType - Video mime type
     */
    playMedia(param) {
        debug.log('play media' + JSON.stringify(param));
        var defer = jQuery.Deferred();

        // stop Nimbus player first
        var player = Nimbus.getPriPlayer();
        if (player) {
            player.stop();
            player.close();
        }

        // create a <video> tag
        var e = this._createH5Player();

        if (param.loop)
            e.loop = true;
        e.muted = false;

        e.src = param.url;

        // wire up events
        e.addEventListener('canplay', function () {
            e.play();
            debug.log('media started');
            $(document).trigger(new $.Event('media_event_received', {eventType: 'play_start'}));
        }, false);

        e.addEventListener('play', function () {
            $(document).trigger(new $.Event('media_event_received', {eventType: 'play_play'}));
        }, false);

        e.addEventListener('pause', function () {
            $(document).trigger(new $.Event('media_event_received', {eventType: 'play_pause'}));
        }, false);

        e.addEventListener('ended', function () {
            $(document).trigger(new $.Event('media_event_received', {eventType: 'play_end'}));
        }, false);

        e.addEventListener('seeked', function () {
            $(document).trigger(new $.Event('media_event_received', {eventType: 'seek_done'}));
        }, false);

        e.addEventListener('error', function () {
            $(document).trigger(new $.Event('media_event_received', {eventType: 'error_in_playing'}));
        }, false);

        // start the video
        defer.resolve();
        return defer.promise();
    }

    /**
     * Stop current playing media
     */
    stopMedia() {
        debug.log('stop media');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            e.pause();
            debug.log('media stopped');
            this._removeH5Player();
            defer.resolve();
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * Pause current playing media
     */
    pauseMedia() {
        debug.log('pause media');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            e.pause();
            debug.log('media pauseed');
            defer.resolve();
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * Resume current playing media
     */
    resumeMedia() {
        debug.log('resume media');
        var defer = jQuery.Deferred();
        var self = this;

        var e = this._getH5Player();
        if (e) {
            e.play();
            debug.log('media resumed');
            defer.resolve();
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * get duration of the video from the TV.
     * duration reported in milliseconds
     */
    getMediaDuration() {
        debug.log('get media duration');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            var dur = parseInt(e.duration * 1000);
            debug.log('media duration: ' + dur + ' ms');
            defer.resolve(dur);
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * Set media position
     * @param positionInMs - position in milliseconds from the beginning
     */
    setMediaPosition(positionInMs) {
        debug.log('set media position to ' + positionInMs + 'ms');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            var pos = positionInMs / 1000;
            if (e.duration > 0 && pos > e.duration) {
                // beyond end, ignored
                debug.log('media position set ignored because ' + pos + 's > ' + e.duration);
            }
            else if (pos < 0) {
                // beyond beginning
                debug.log('media position set ignored because ' + pos + 's < 0');
            }
            else {
                e.currentTime = positionInMs / 1000;
                debug.log('media position set to ' + positionInMs + ' ms');
            }
            defer.resolve();
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * Get media position
     */
    getMediaPosition() {
        debug.log('get media position');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            var pos = parseInt(e.currentTime * 1000);
            debug.log('media position currently is ' + pos + 'ms');
            defer.resolve(pos);
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    getMediaPaused() {
        debug.log('is media paused');
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            var paused = e.paused;
            debug.log('media paused: ' + paused);
            defer.resolve(paused);
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    setMediaMute(muted) {
        debug.log('set media mute: ' + muted);
        var defer = jQuery.Deferred();

        var e = this._getH5Player();
        if (e) {
            e.muted = !!muted;
            defer.resolve();
        }
        else {
            defer.reject('Cannot find video tag');
        }

        return defer.promise();
    }

    /**
     * Set the video layer size
     * @param param object containing values
     * @param param.x - x position of video
     * @param paramy.y - y position of video
     * @param param.width - width of video
     * @param param.height - height ov video
     */
    setVideoLayerSize(param) {
        var player = Nimbus.getPriPlayer();
        var e = this._getH5Player();
        if (player) {
            debug.log('set native player size to ' + JSON.stringify(param));
            player.setVideoLayerRect(param.x, param.y, param.width, param.height);
        }
        else if (e) {
            debug.log('set HTML5 layer size to ' + JSON.stringify(param));
            e.parentElement.style.left = param.x + 'px';
            e.parentElement.style.top = param.y + 'px';
            e.parentElement.style.width = param.width + 'px';
            e.parentElement.style.height = param.height + 'px';
        }
    }

    /**
     * Get the video layer size
     */
    getVideoLayerSize() {
        var player = Nimbus.getPriPlayer();
        var e = this._getH5Player();
        if (player) {
            debug.log('get native player size');
            var rect = player.getVideoLayerRect();
            return rect;
        }
        else if (e) {
            debug.log('get HTML5 layer size');
            var rect = {};
            rect.x = parseInt(e.parentElement.style.left);
            rect.y = parseInt(e.parentElement.style.top);
            rect.width = parseInt(e.parentElement.style.width);
            rect.height = parseInt(e.parentElement.style.height);
            return rect;
        }
    }

    /**
     * Display video layer
     */
    showVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: 'transparent'});
    }

    /**
     * Hide video layer
     */
    hideVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: ''});
    }

    /**
     * Set local time (for devices that does not uses NTP client)
     * @param {Object} param - Parameter object
     * @param {Date} param.date
     * @param {Number} param.year
     * @param {Number} param.month
     * @param {Number} param.day
     * @param {Number} param.hour
     * @param {Number} param.minute
     * @param {Number} param.second
     * @param {Number} param.gmtOffsetInMinute
     * @param {Number} param.isDaylightSaving
     */
    setTime(param) {
        debug.log('unsupported');
    }

    /**
     * Set local timezone
     */
    /**
     * Set local timezone
     * @param {string} param.name - Timezone name. eg: "Amercia/Los_Angles"
     * @param {string} param.posix - Timezone posix format. eg: "EST5EDT"
     */
    setTimezone(param) {
        Nimbus.setTimeZoneMode("Name");
        Nimbus.setTimeZoneName(param.name, param.posix);
    }

    reload(param) {
        Nimbus.reload(true);
    }

    reboot(param) {
        Nimbus.reboot();
    }

    setExternalInput(input) {
        var defer = jQuery.Deferred();
        var inputObj = inputMap[input];

        if (!inputObj) {
            defer.reject("Invalid input string: " + input);
        }
        else {
            debug.log('switching input to type:' + inputObj + ' ...');
            var ctrl = Nimbus.getTVController();
            if (ctrl) {
                ctrl.setInput(inputObj);
                defer.resolve();
            }
            else {
                defer.reject('Unable to obtain tv controller');
            }
        }
        return defer.promise();
    }

    getExternalInput() {
        var defer = jQuery.Deferred();
        var ctrl = Nimbus.getTVController();
        if (ctrl) {
            var o = ctrl.getInput();
            debug.log('Current input: ' + JSON.stringify(o));
            var id = reverseInputMap[o.ID];
            if (id) {
                defer.resolve(id);
            }
            else {
                defer.reject('Invalid input');
            }
        }
        else {
            defer.reject('Unable to obtain tv controller');
        }
        return defer.promise();
    }

    getStartChannel() {
        debug.log('unsupported');
    }

    setStartChannel(startChannel) {
        debug.log('unsupported');
    }
}

const inputMap = {}, reverseInputMap = {};

inputMap['TV'] = 'Tuner';
inputMap['Tuner'] = 'Tuner';
inputMap['HDMI'] = 'DVI/HDMI0';
inputMap['HDMI1'] = 'DVI/HDMI1';
inputMap['HDMI2'] = 'DVI/HDMI2';
inputMap['HDMI3'] = 'DVI/HDMI3';
inputMap['Component'] = inputMap['Component1'] = 'Component0';
inputMap['RGB'] = inputMap['RGB1'] = 'VGA/RGB0';
inputMap['Composite'] = inputMap['Composite1'] = 'Composite0';

reverseInputMap['Tuner'] = 'TV';
reverseInputMap['DVI/HDMI0'] = 'HDMI1';
reverseInputMap['DVI/HDMI1'] = 'HDMI2';
reverseInputMap['DVI/HDMI2'] = 'HDMI3';
reverseInputMap['Component0'] = 'Component';
reverseInputMap['VGA/RGB0'] = 'RGB';
reverseInputMap['Composite0'] = 'Composite';