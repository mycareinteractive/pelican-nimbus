// use script-loader to load nimbus library
require('script!../vendors/enseo_nimbus.js');
require('script!../vendors/enseo_player.js');
require('script!../vendors/enseo_tvcontroller.js');
require('script!../vendors/enseo_browser.js');

var $ = require('jquery');
var Radio = require('backbone.radio');
var NimbusDevice = require('../src/nimbus').default;

window.jQuery = window.$ = $;

window.PelicanDevice = new NimbusDevice();
$(document).one('deviceready', onDeviceReady);
$(document).on('deviceprogress', onDeviceProgress);
$(document).on('devicefail', onDeviceFail);
$(document).on('media_event_received', onMediaEvent);

var channel = Radio.channel('device');
channel.on('key', keyHandler);

window.onload = function () {
    window.PelicanDevice.bootstrap();
};

function addMessage(msg) {
    $('<li/>').text(msg).appendTo('#messages');
    $('#messages').scrollTop($('#messages').prop("scrollHeight"));
}

function onDeviceReady(e) {
    addMessage("Nimbus is ready - Don't worry love, cavalry's here!");
    addMessage('====================================================================================');
    console.log('Nimbus device ready');
    PelicanDevice.startHandshake('Aceso');
    addMessage('Platform: ' + PelicanDevice.getPlatform());
    addMessage('Hardware: ' + PelicanDevice.getHardwareModel() + ' ' + PelicanDevice.getHardwareVersion());
    addMessage('Serial: ' + PelicanDevice.getHardwareSerial());
    addMessage('Firmware: ' + PelicanDevice.getMiddlewareVersion());
    addMessage('Power: ' + PelicanDevice.getPower());
    addMessage('Volume: ' + PelicanDevice.getVolume());
    addMessage('Network Interfaces: ');
    var nets = PelicanDevice.getNetworkDevices();
    $.each(nets, function(i, n) {
        addMessage('-- ' + n.name + ': mac ' + n.mac + ' ip ' + n.ip + ' mask ' + n.netmask + ' gateway ' + n.gateway);
    });
    playVideo();
}

function onDeviceProgress(e) {
    addMessage('Progress: ' + e.data);
}

function onDeviceFail(e) {
    console.log('Nimbus device failed');
    addMessage('Fail: ' + e.data);
}

function onMediaEvent(e) {
    addMessage('Media event: ' + e.eventType);
}

function keyHandler(e, key) {
    addMessage('Key press: (' + e.keyCode + ') ' + key);

    if (key == 'ENTER') {
        PelicanDevice.getMediaPaused().done(function (paused) {
            if (paused)
                PelicanDevice.resumeMedia();
            else
                PelicanDevice.pauseMedia();
        });
    }
    else if (key == 'LEFT') {
        PelicanDevice.getMediaPosition().done(function (pos) {
            PelicanDevice.setMediaPosition(pos - 10000);
        });
    }
    else if (key == 'RIGHT') {
        PelicanDevice.getMediaPosition().done(function (pos) {
            PelicanDevice.setMediaPosition(pos + 10000);
        });
    }
    else if (key == 'DOWN') {
        PelicanDevice.stopMedia();
    }
    else if (key == 'UP') {
        playVideo();
    }
}

function playVideo() {
    PelicanDevice.playMedia(
        {
            url: 'https://s3-us-west-1.amazonaws.com/acesodemo/content/bigbuckbunny_720p.mov'
        }).done(function () {

    });
    PelicanDevice.setVideoLayerSize({
        x: 0,
        y: 0,
        width: 1280,
        height: 720
    });
    var o = Nimbus.getBrowserMemoryUsage();
    addMessage('Memory: ' + JSON.stringify(o));
}