var path = require('path');
var ES5to3OutputPlugin = require("es5to3-webpack-plugin");

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, '');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './tests/test.js'
    ],

    output: {
        filename: 'pelican-nimbus.test.min.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {
                test: /\.js(x)?$/,
                exclude: /(node_modules|vendors)/,
                loaders: ['babel'],
                plugins: [
                    'transform-es3-property-literals',
                    'transform-es3-member-expression-literals'
                ]
            }
        ]
    },

    devServer: {
        // Note Opera 10.7 doesn't support HMR (--hot and --inline)
        disableHostCheck: true,
        host: "0.0.0.0",
        port: 3000
    },

    plugins: [
        new ES5to3OutputPlugin()
    ]
};

module.exports = config;